# Use the cargo-lambda image for building
FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

# Create a directory for your application
WORKDIR /usr/src/app

# Copy your source code into the container
COPY . .

# Build the Lambda function using cargo-lambda
RUN cargo lambda build --release --arm64

# Use a new stage for the final image
# Use the AWS provided base image for Lambda that matches the target architecture
FROM public.ecr.aws/lambda/provided:al2-arm64

# Create a directory for your lambda function
WORKDIR /var/task

# Copy the bootstrap binary from the builder stage
# Ensure the path matches where cargo-lambda outputs the bootstrap binary
COPY --from=builder /usr/src/app/target/lambda/release/bootstrap /var/task/bootstrap

# Copy the model file from the builder stage
# Adjust the source path according to where the model is actually located
COPY --from=builder /usr/src/app/src/pythia-1b-q4_0-ggjt.bin /var/task/pythia-1b-q4_0-ggjt.bin

# (Optional) Check to make sure files are there
RUN if [ -f /var/task/bootstrap ]; then echo "Bootstrap file exists"; else echo "Bootstrap file does not exist"; fi
RUN if [ -f /var/task/pythia-1b-q4_0-ggjt.bin ]; then echo "Model file exists"; else echo "Model file does not exist"; fi

# Set permissions for the bootstrap executable
RUN chmod 755 /var/task/bootstrap

# Set the entrypoint for the Lambda function
ENTRYPOINT ["/var/task/bootstrap"]
