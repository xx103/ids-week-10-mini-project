# IDS Week 10 Mini-Project: Deploying Hugging Face Rust Transformer to AWS Lambda

This guide details the entire process for setting up, dockerizing, and deploying a Hugging Face Rust transformer to AWS Lambda using an Amazon Elastic Container Registry (ECR) image. The application provides an inference service for language models, accepting queries via HTTP and returning model predictions.

### Requirements

- Docker installed on your system
- AWS CLI configured with necessary permissions
- Git LFS installed for handling large files
- Rust development environment

### Project Overview

The Rust application (`main.rs`) creates an HTTP server to handle inference requests. It uses the transformer's model architecture and tokenizer to generate responses based on prompts received via the query endpoint.

#### Key Components

- **Args struct**: Parses command line arguments specifying model and tokenizer details.
- **infer function**: Loads the model and performs inference based on the prompt received.
- **function_handler**: Handles HTTP requests, extracting the prompt and sending it to the `infer` function, then returns the model's response.
- **main function**: Sets up the asynchronous runtime and starts the HTTP server.


### Implementation of Query Endpoint in main.rs

The `main.rs` file in the Rust application uses `lambda_http` to handle HTTP requests and perform inference with a machine learning model. Below is a detailed explanation of its components and functionalities.

### Imports and Setup

- **Imports**: Essential modules and structs such as `lambda_http`, `clap`, and `std::path::PathBuf` are imported.
- **Struct Definitions**: The `Args` struct, defined with `clap`, parses command-line arguments for model and tokenizer configurations.

### Function Definitions

1. **Tokenizer Source Configuration**:
   - The `to_tokenizer_source` method resolves the source of the tokenizer based on command-line arguments, ensuring that either a path or a repository is used.

2. **Inference Function (`infer`)**:
   - **Parsing Arguments**: Parses arguments to configure the tokenizer and model.
   - **Model Loading**: Dynamically loads the model with the specified architecture and tokenizer source.
   - **Session Initialization**: Starts a session for the model.
   - **Inference Execution**: Performs inference using the provided prompt, capturing output tokens into a result string.
   - **Error Handling**: Catches and handles any errors during inference.

### Lambda Function Handler

- **Request Handling**: The `function_handler` extracts the `prompt` from query parameters and passes it to the `infer` function.
- **Response Generation**:
  - **Successful Inference**: Returns inference output in the body of a 200 OK HTTP response.
  - **Error Handling**: Returns a 500 Internal Server Error response with an error message if inference fails.

### Main Function

- **Server Initialization**: Sets up an asynchronous runtime and starts the HTTP server, registering `function_handler` to handle requests.
- **Running the Service**: Uses `lambda_http`'s `run` function to adapt `function_handler` for AWS Lambda's request handling structure.


### Step-by-Step Guide

#### Step 1: Prepare Your Development Environment

Ensure you have Rust and Cargo installed, along with Docker for containerization. Also, set up Git LFS to handle large model files.

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup toolchain install stable
rustup update
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
git lfs install
```

#### Step 2: Project Setup
- Create a new project with `cargo lambda new project_name`.
- Select a model from  https://huggingface.co/rustformers
- Update `main.rs`,`Cargo.toml`, and add ` pythia-410m-q4_0-ggjt.bin`
- Use the following commands to test locally:
        ```bash
        cargo lambda watch
        cargo lambda build
        ```
        
#### Step 3: AWS Configuration

1. **Create an IAM User**:
   - Log in to your AWS account, navigate to the AWS IAM console.
   - add an IAM User for credentials.
   - Attach the `AmazonEC2ContainerRegistryFullAccess`, `LambdaFullAccess` and `IAMFullAccess` policies.
   - Note down the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.
   - Add a `.env` file with your AWS credentials and region, and ensure it's listed in `.gitignore`.

2. **Configure AWS CLI**:
   - Run `aws configure` to set your `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`.


#### Step 3: Dockerization and ECR Setup

1. **Open Terminal and Navigate to Your Project Directory**
   - Ensure you are in the directory where your Dockerfile is located. Use the `cd` command:
     ```sh
     cd path/to/your/project
     ```
   - Replace `path/to/your/project` with the actual path where your Dockerfile resides.

2. **Log in to AWS ECR**
   - Before building the image, ensure you are logged in to your AWS ECR:
     ```sh
     aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin< AWS account ID>.dkr.ecr.us-east-1.amazonaws.com
     ```

3. **Build the Docker Image with the Correct Platform**
   - Use the following command to build an image compatible with the `linux/arm64` architecture:
     ```sh
     docker buildx build --progress=plain --platform linux/arm64 -t < AWS account ID>.dkr.ecr.us-east-1.amazonaws.com/my-lambda-function:latest .
     ```
   - This command will:
     * `--progress=plain`: Show plain progress output.
     * `--platform linux/arm64`: Specify the target platform for the build.
     * `-t`: Tag the image. Replace `< AWS account ID>` with your actual AWS account ID, `us-east-1` with your AWS region, and `my-lambda-function` with the name of your ECR repository.
     * `.`: Use the current directory as the build context.

4. **Verify the Image Build**
   - Check the output for any errors after running the build command. A successful build will indicate no errors.

5. **Push the Image to ECR**
   - Once the build is successful, push the image to your AWS ECR:
     ```sh
     docker push < AWS account ID>.dkr.ecr.us-east-1.amazonaws.com/my-lambda-function:latest
     ```
   - This command pushes the image tagged as `latest` to your ECR repository.

6. **Confirm the Image in ECR**
   - After pushing the image, visit the ECR console in AWS and navigate to your repository. You should see the `latest` image listed there.

### Additional Considerations

- **Buildx Setup**: If `docker buildx` isn't set up on your machine, you might need to create and use a new build instance:
  ```sh
  docker buildx create --name mybuilder --use
  docker buildx inspect --bootstrap
  ```

- **Build Kit**: Enable Docker's build kit for `buildx` to work properly by setting the environment variable:
  ```sh
  export DOCKER_BUILDKIT=1
  ```

- **Ensure Docker Hub Access**: If you need access to private images or certain base images, make sure you are logged into Docker Hub:
  ```sh
  docker login
  ```

### General Docker Build Command

Assuming your multi-stage Dockerfile is set up correctly, your build command should look like this:
```sh
docker buildx build --progress=plain --platform linux/arm64 -t < AWS account ID>.dkr.ecr.us-east-1.amazonaws.com/my-lambda-function:latest .
```
Ensure that each stage in your Dockerfile is defined correctly and that you have access to all the base images used in your Dockerfile.


#### Step 4: Deploy to AWS Lambda

1. **Create a Lambda Function**:
   - Open the AWS Management Console and navigate to the Lambda service.
   - Click on `Create function`.
   - Choose `Container image` as the source of your Lambda function.
   - Enter a name for your function.
   - Click on `Browse images`, and select the image you pushed to ECR.
   - Click on `Create function`.

2. **Configure Lambda Settings**:
   - Once your function is created, navigate to the `Configuration` tab.
   - Under `General configuration`, click on `Edit`.
   - Set the memory to 3008 MB. This is the maximum allowed for new users and should accommodate the demands of most machine learning models.
   - Set the timeout to 10 minutes. This extended timeout is necessary due to the potential long processing time of deep learning models.
   - Save the changes.

3. **Set Up Function URL**:
   - Still in the `Configuration` tab, find the `Function URL` section.
   - Click on `Create function URL`.
   - Set the Auth type to `NONE` to allow unauthenticated access for testing purposes.
   - Enable CORS if your client-side application requires it to call this function.
   - Save the Function URL for use in testing.

#### Step 5: Testing

1. **Invoke the Function**:
   - Open a tool capable of sending HTTP requests, such as Postman or use `curl` in your terminal.
   - Construct an HTTP GET or POST request with the appropriate payload. If using `curl`, the command might look like this:
     ```bash
     curl -X POST -H "Content-Type: application/json" -d '{"prompt":"Hello, world!"}' [Function URL]
     ```
   - Replace `[Function URL]` with the actual URL you obtained after setting up the function URL.
   - Send the request. The Lambda function should process the input and return a response derived from the model inference based on the provided prompt.

2. **Monitor and Debug**:
   - Go back to the AWS Lambda console to monitor the execution in the `Monitor` tab.
   - Review logs in Amazon CloudWatch to troubleshoot any issues during the invocation.
   - Adjust configurations and test again if necessary to ensure the function operates as expected.

#### Step 6: Version Control

Ensure your model files (.bin) are tracked using Git LFS:

```bash
git lfs track "*.bin"
git add .gitattributes
git add your_model_file.bin
git commit -m "Add model file with LFS"
git push
```
