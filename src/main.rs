use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};
use std::path::PathBuf;
use clap::Parser;
use std::io::Write;
use std::convert::Infallible;


#[derive(Parser)]
struct Args {
    #[clap(long)]
    model_architecture: llm::ModelArchitecture,
    #[clap(long, default_value = "src/pythia-410m-q4_0-ggjt.bin")]
    model_path: PathBuf,
    #[clap(long, short = 'p')]
    prompt: Option<String>,
    #[clap(long, short = 'v')]
    pub tokenizer_path: Option<PathBuf>,
    #[clap(long, short = 'r')]
    pub tokenizer_repository: Option<String>,
}

impl Args {
    pub fn to_tokenizer_source(&self) -> llm::TokenizerSource {
        match (&self.tokenizer_path, &self.tokenizer_repository) {
            (Some(_), Some(_)) => {
                panic!("Cannot specify both --tokenizer-path and --tokenizer-repository");
            },
            (Some(path), None) => llm::TokenizerSource::HuggingFaceTokenizerFile(path.to_owned()),
            (None, Some(repo)) => llm::TokenizerSource::HuggingFaceRemote(repo.to_owned()),
            (None, None) => llm::TokenizerSource::Embedded,
        }
    }
}

async fn infer(prompt: String) -> Result<String, Box<dyn std::error::Error>> {
    let args = Args::parse();
    let tokenizer_source = args.to_tokenizer_source();
    let model_architecture = args.model_architecture;
    let model_path = args.model_path;

    let model = llm::load_dynamic(
        Some(model_architecture),
        &model_path,
        tokenizer_source,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;

    let mut session = model.start_session(Default::default());
    let mut generated_tokens = String::new();

    let res = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &llm::InferenceRequest {
            prompt: (&prompt).into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(20),
        },
        &mut Default::default(),
        |r| match r {
            llm::InferenceResponse::PromptToken(t) | llm::InferenceResponse::InferredToken(t) => {
                print!("{t}");
                std::io::stdout().flush().unwrap();
                generated_tokens.push_str(&t);
                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Continue),
        },
    );

    match res {
        Ok(_) => Ok(generated_tokens),
        Err(err) => Err(Box::new(err)),
    }
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Create an owned variable to hold the result.
    let query_params = event.query_string_parameters();
    let prompt = query_params.first("prompt").unwrap_or_default().to_owned();

    match infer(prompt).await {
        Ok(inference_result) => {
            let response = Response::builder()
                .status(200)
                .header("Content-Type", "text/plain")
                .body(inference_result.into())
                .expect("Failed to render response");
            Ok(response)
        },
        Err(e) => {
            let error_message = format!("Error during inference: {:?}", e);
            let response = Response::builder()
                .status(500)
                .header("Content-Type", "text/plain")
                .body(error_message.into())
                .expect("Failed to render response");
            Ok(response)
        }
    }
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await
}